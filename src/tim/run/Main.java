package tim.run;

import java.awt.EventQueue;

import tim.controller.MainViewController;

public class Main {
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainViewController mainViewController = new MainViewController();
					mainViewController.getMainView().setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
