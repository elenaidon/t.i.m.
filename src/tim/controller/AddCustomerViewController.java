package tim.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;

import javax.swing.JOptionPane;

import tim.code.ManageCustomer;
import tim.forms.AddCustomerView;

public class AddCustomerViewController {
	private AddCustomerView addCustomerView = new AddCustomerView();

	public AddCustomerView getAddCustomerView() {
		return addCustomerView;
	}

	public AddCustomerViewController() {
		addCustomerView.addWindowFocusListener(new WindowFocusListener() {
			public void windowGainedFocus(WindowEvent e) {
				addCustomerView.setLocationRelativeTo(null);
			}

			public void windowLostFocus(WindowEvent e) {
			}
		});
		addCustomerView.addBtnResetActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resetTextFields();
			}
		});
		addCustomerView.addBtnAddActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (isInfoValid()) {
					/**
					 * Adds a new customer to the database.
					 */
					new ManageCustomer().addCustomer(addCustomerView
							.getTxtFirstName(),
							addCustomerView.getTxtLastName(), 0,
							Double.parseDouble(addCustomerView.getTxtChargePerHour()));
					resetTextFields();
					addCustomerView.dispose();
				} else {
					showErrorDialog("Invalid info.");
				}
			}
		});
		addCustomerView.addBtnCancelActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resetTextFields();
				addCustomerView.dispose();
			}
		});
	}

	private void resetTextFields() {
		addCustomerView.setTxtFirstName(null);
		addCustomerView.setTxtLastName(null);
		addCustomerView.setTxtChargePerhour(null);
	}

	private boolean isInfoValid(){
		if (addCustomerView.getTxtFirstName().isEmpty()
				|| addCustomerView.getTxtLastName().isEmpty()
				|| addCustomerView.getTxtChargePerHour().isEmpty()) {
			return false;
		} else {
			return true;
		}
	}

	private void showErrorDialog(String message) {
		JOptionPane.showMessageDialog(null, message, "Error",
				JOptionPane.ERROR_MESSAGE);
	}
}
