package tim.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;

import javax.swing.JOptionPane;
import javax.swing.Timer;

import tim.code.Customer;
import tim.code.ManageCustomer;
import tim.code.Stopwatch;
import tim.forms.MainView;

public class MainViewController {
	public final static String euro = "\u20ac";
	private static final String stop = "Stop";
	private static final String start = "Start";

	private MainView mainView = new MainView();
	private CustomersViewController customersViewController = new CustomersViewController();

	private Stopwatch stopwatch = new Stopwatch();
	private ClockListener clockListener;
	
	public MainView getMainView() {
		return mainView;
	}

	public MainViewController() {
		/**
		 * Initialize currentCustomer.
		 */
		customersViewController.getCustomersView().setCurrentCustomer(
				new Customer());
		resetTimer();
		/**
		 * Updates CurrentCustomer Labels.
		 */
		mainView.setCurrentCustomerFullName(customersViewController
				.getCustomersView().getCurrentCustomer()
				.getFullName());
		mainView.setCurrentCustomerTimeWorked(new Stopwatch()
				.getElapsedTimeStringFromMillis(customersViewController
						.getCustomersView()
						.getCurrentCustomer().getElapsedTimeInMillis()));
		mainView.setTglBtnStartText(start);

		mainView.addWindowFocusListener(new WindowFocusListener() {
			public void windowGainedFocus(WindowEvent e) {
				String customerFullName = customersViewController
						.getCustomersView()
						.getCurrentCustomer().getFullName();
				mainView.setCurrentCustomerFullName(customerFullName);

				String customerTimeWorked = new Stopwatch()
						.getElapsedTimeStringFromMillis(customersViewController
								.getCustomersView()
						.getCurrentCustomer()
						.getElapsedTimeInMillis());
				mainView.setCurrentCustomerTimeWorked(customerTimeWorked);
				
				String customerChargePerHour = String
						.valueOf(customersViewController.getCustomersView()
						.getCurrentCustomer().getChargePerHour()) + " " + euro;
				mainView.setCurrentCustomerChargePerHour(customerChargePerHour);

				String customerDebt = String.valueOf(customersViewController
						.getCustomersView()
						.getCurrentCustomer().getDebt());
				mainView.setCurrentCustomerDebt(customerDebt + " " + euro);
			}

			public void windowLostFocus(WindowEvent arg0) {
			}
		});
		mainView.addBtnManageCustomersActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				customersViewController.getCustomersView().setVisible(true);
			}
		});
		mainView.addBtnSaveActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (isThereElapsedTimeToSave()) {
					if (mainView.isTimerRunning()){
						showErrorDialog("Stopwatch is still running.");
					} else {
						/**
						 * Adds the elapsed time to the existing time worked for
						 * the current customer.
						 */
						long finalElapsedMillis = customersViewController
								.getCustomersView()
								.getCurrentCustomer().getElapsedTimeInMillis()
								+ stopwatch.getElapsedTimeInMillis();

						customersViewController.getCustomersView()
								.getCurrentCustomer()
								.setElapsedTimeInMillis(
								finalElapsedMillis);
						/**
						 * Updates time worked for current customer in the
						 * database.
						 */
						new ManageCustomer().updateCustomerElapsedMillis(
								customersViewController.getCustomersView()
								.getCurrentCustomer().getId(), finalElapsedMillis);
						/**
						 * Updates current customers labels.
						 */
						String currentCustomerTimeWorked = new Stopwatch()
								.getElapsedTimeStringFromMillis(customersViewController
										.getCustomersView()
										.getCurrentCustomer()
										.getElapsedTimeInMillis());
						mainView.setCurrentCustomerTimeWorked(currentCustomerTimeWorked);
						/**
						 * Re-calculates the Debt and updates the debt label.
						 */
						String currentCustomerDebt = String
								.valueOf(customersViewController
										.getCustomersView()
										.getCurrentCustomer()
										.getDebt());
						mainView.setCurrentCustomerDebt(currentCustomerDebt
								+ " " + euro);

						resetTimer();
						mainView.setElapsedTimeString(stopwatch
								.getElapsedTimeFormatedString());
					}
				} else {
					showErrorDialog("There is nothing to save");
				}
			}
		});
		mainView.addTglBtnStartItemListener(new ItemListener() {
			/**
			 * If the state changes to start the timer starts running. If the
			 * state changes to stop it stops. It also changes the toggle
			 * button's text accordingly.
			 */
			public void itemStateChanged(ItemEvent e) {
				if (mainView.isTglBtnStartSelected()) {
					if (mainView.getLblCurrentCustomerFullNameText()
							.equalsIgnoreCase(new Customer().getFullName())) {
						showErrorDialog("You can't start the timer without choosing a customer.");
					} else {
						mainView.startTimer();
						mainView.setTglBtnStartText(stop);
					}

				} else {
					mainView.stopTimer();
					mainView.setTglBtnStartText(start);
				}
			}
		});
		mainView.addBtnResetActionListener(new ActionListener() {
			/**
			 * If the stopwatch is paused it resets it.
			 */
			public void actionPerformed(ActionEvent e) {
				if (mainView.isTimerRunning()) {
					JOptionPane.showMessageDialog(null,
							"Stopwatch is still running.", "Error",
							JOptionPane.ERROR_MESSAGE);
				} else {
					resetTimer();
					mainView.setElapsedTimeString(stopwatch
							.getElapsedTimeFormatedString());
				}
			}
		});
		mainView.addTimerActionListener(clockListener);
		

	}

	private void createNewClockListener() {
		clockListener = new ClockListener() {
			/**
			 * Defines what happens every 1000 milliseconds the timer is
			 * running.
			 */
			@Override
			public void actionPerformed(ActionEvent e) {

				stopwatch.setElapsedTimeInMillis(count * 1000);
				mainView.setElapsedTimeString(stopwatch
						.getElapsedTimeFormatedString());
				count++;
			}
		};
	}

	public Stopwatch getStopwatch() {
		return stopwatch;
	}

	public void setStopwatch(Stopwatch stopwatch) {
		this.stopwatch = stopwatch;
	}

	public void resetTimer(){
		createNewClockListener();
		Timer tempTimer = new Timer(1000, clockListener);
		tempTimer.setInitialDelay(0);
		mainView.setTimer(tempTimer);
		stopwatch = new Stopwatch();
	}

	/**
	 * Checks if the stopwatch did run or not.
	 */
	private boolean isThereElapsedTimeToSave() {
		return !mainView.getElapsedTimeString().equals(
				new Stopwatch().getElapsedTimeFormatedString());
	}

	private void showErrorDialog(String message) {
		JOptionPane.showMessageDialog(null, message, "Error",
				JOptionPane.ERROR_MESSAGE);
	}

}
