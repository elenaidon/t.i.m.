package tim.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;

import tim.code.Customer;
import tim.code.ManageCustomer;
import tim.code.Stopwatch;
import tim.forms.CustomersView;

public class CustomersViewController {
	private CustomersView customersView = new CustomersView();
	private AddCustomerViewController addCustomerViewController = new AddCustomerViewController();
	private EditCustomerViewController editCustomerViewController = new EditCustomerViewController();
	
	private final static String[] columnTitles = new String[] { "Customer Id",
			"Last Name", "First Name", "DD:HH:MM:SS", "Charge/Hour" };
	public final static String euro = "\u20ac";

	public CustomersView getCustomersView() {
		return customersView;
	}

	public CustomersViewController() {
		customersView.getCustomerTable().setModel(createTableModel());

		customersView.setCurrentCustomer(new Customer());

		customersView.addWindowFocusListener(new WindowFocusListener() {
			public void windowGainedFocus(WindowEvent e) {
				customersView.setLocationRelativeTo(null);
				customersView.setLblCurrentCustomerName(customersView
						.getCurrentCustomer().getFullName());

				filterTableFromTextFieldText();
			}

			public void windowLostFocus(WindowEvent e) {
			}
		});
		customersView
				.addTxtSearchCustomerDocumentListener(new DocumentListener() {

					@Override
					public void changedUpdate(DocumentEvent e) {
						filterTableFromTextFieldText();
					}

					@Override
					public void insertUpdate(DocumentEvent e) {
						filterTableFromTextFieldText();
					}

					@Override
					public void removeUpdate(DocumentEvent e) {
						filterTableFromTextFieldText();
					}

				});
		customersView.addBtnAddCustomerActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addCustomerViewController.getAddCustomerView().setVisible(true);
			}
		});
		customersView.addBtnRemoveCustomerActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (customersView.getCustomerTable().getSelectedRow() == -1) {
					showErrorDialog("You didn't select a customer.");
				}
				int selectedCustomerId = Integer
						.parseInt((String) customersView.getCustomerTable().getValueAt(
								customersView.getCustomerTable().getSelectedRow(), 0));
				new ManageCustomer().deleteCustomer(selectedCustomerId);
				filterTableFromTextFieldText();
			}
		});
		customersView.addBtnEditCustomerActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (customersView.getCustomerTable().getSelectedRow() == -1) {
					showErrorDialog("You didn't select a customer.");
				}
				int selectedCustomerId = Integer
						.parseInt((String) customersView.getCustomerTable().getValueAt(
								customersView.getCustomerTable().getSelectedRow(), 0));
				customersView.setCurrentCustomer(new ManageCustomer()
						.getCustomerById(selectedCustomerId));
				editCustomerViewController.getEditCustomerView()
						.setSelectedCustomerId(selectedCustomerId);
				editCustomerViewController.getEditCustomerView().setVisible(
						true);
			}
		});
		customersView.addBtnSelectCustomerActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (customersView.getCustomerTable().getSelectedRow() == -1) {
					showErrorDialog("You didn't select a customer.");
				}
				int selectedCustomerId = Integer
						.parseInt((String) customersView.getCustomerTable().getValueAt(
								customersView.getCustomerTable().getSelectedRow(), 0));
				customersView.setCurrentCustomer(new ManageCustomer()
						.getCustomerById(selectedCustomerId));
				customersView.setVisible(false);
			}
		});
		customersView.addBtnCancelActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				customersView.setVisible(false);
			}
		});
	}

	private DefaultTableModel createTableModel() {
		@SuppressWarnings("serial")
		DefaultTableModel tempModel = new DefaultTableModel() {
			@Override
			public boolean isCellEditable(int row, int column) {
				// all cells false
				return false;
			}
		};
		tempModel.setColumnIdentifiers(columnTitles);
		populateTableWithCustomers(new ManageCustomer().getCustomerList());
		return tempModel;
	}

	public void populateTableWithCustomers(List<Customer> customerList) {
		for (Customer tempCustomer : customerList) {
			((DefaultTableModel) customersView.getCustomerTable().getModel())
					.addRow(new String[] {
					Integer.toString(tempCustomer.getId()),
					tempCustomer.getLastName(),
					tempCustomer.getFirstName(),
					new Stopwatch().getElapsedTimeStringFromMillis(tempCustomer
							.getElapsedTimeInMillis()),
					String.valueOf(tempCustomer.getChargePerHour()) + " "
							+ euro });
		}
	}

	public void filterTableFromTextFieldText() {
		DefaultTableModel model = new DefaultTableModel();
		model.setColumnIdentifiers(new String[] { "Customer Id", "Last Name",
				"First Name", "DD:HH:MM:SS", "Charge/Hour" });
		List<Customer> customersList = new ManageCustomer()
				.findCustomer(customersView.getTxtSearchCustomer());
		for (Customer tempCustomer : customersList) {
			model.addRow(new String[] {
					Integer.toString(tempCustomer.getId()),
					tempCustomer.getLastName(),
					tempCustomer.getFirstName(),
					new Stopwatch().getElapsedTimeStringFromMillis(tempCustomer
							.getElapsedTimeInMillis()),
					String.valueOf(tempCustomer.getChargePerHour()) + " "
							+ euro });
		}
		customersView.getCustomerTable().setModel(model);
	}

	private void showErrorDialog(String message) {
		JOptionPane.showMessageDialog(null, message, "Error",
				JOptionPane.ERROR_MESSAGE);
	}
}
