package tim.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;

import javax.swing.JOptionPane;

import tim.code.Customer;
import tim.code.ManageCustomer;
import tim.code.Stopwatch;
import tim.forms.EditCustomerView;

public class EditCustomerViewController {
	private EditCustomerView editCustomerView = new EditCustomerView();

	public final static String euro = "\u20ac";

	public EditCustomerView getEditCustomerView() {
		return editCustomerView;
	}

	public EditCustomerViewController() {
		editCustomerView.addWindowFocusListener(new WindowFocusListener() {
			public void windowGainedFocus(WindowEvent e) {
				editCustomerView.setLocationRelativeTo(null);
				Customer selectedCustomer = new ManageCustomer()
						.getCustomerById(editCustomerView
								.getSelectedCustomerId());
				populateTxtFieldsWIthCustomerInfo(selectedCustomer);
			}

			public void windowLostFocus(WindowEvent e) {
			}
		});
		editCustomerView.addBtnSaveActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (isInfoValid()) {
				Customer selectedCustomer = new ManageCustomer()
						.getCustomerById(editCustomerView
								.getSelectedCustomerId());
				new ManageCustomer().updateCustomerInfo(selectedCustomer
						.getId(), editCustomerView.getTxtFirstName(),
						editCustomerView.getTxtLastName(), Double
								.parseDouble(editCustomerView
										.getTxtChargePerHour()));
				selectedCustomer = new ManageCustomer()
				.getCustomerById(editCustomerView
						.getSelectedCustomerId());
				populateTxtFieldsWIthCustomerInfo(selectedCustomer);
				} else {
					showErrorDialog("Invalid info.");
				}
			}
		});
		editCustomerView
				.addBtnResetTimeWorkedActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						Customer selectedCustomer = new ManageCustomer()
								.getCustomerById(editCustomerView
										.getSelectedCustomerId());
						new ManageCustomer().updateCustomerElapsedMillis(
								selectedCustomer.getId(), 0);
						selectedCustomer = new ManageCustomer()
								.getCustomerById(editCustomerView
										.getSelectedCustomerId());
						populateTxtFieldsWIthCustomerInfo(selectedCustomer);
					}
				});
		editCustomerView.addBtnCancelActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				editCustomerView.setTxtFirstName(null);
				editCustomerView.setTxtLastName(null);
				editCustomerView.setTxtTimeWorked(null);
				editCustomerView.setTxtChargePerHour(null);
				editCustomerView.dispose();
			}
		});
	}

	/**
	 * Checks if the user is trying to pass the form empty.
	 */
	private boolean isInfoValid() {
		if (editCustomerView.getTxtFirstName().isEmpty()
				|| editCustomerView.getTxtLastName().isEmpty()
				|| editCustomerView.getTxtChargePerHour().isEmpty()) {
			return false;
		} else {
			return true;
		}
	}

	private void showErrorDialog(String message) {
		JOptionPane.showMessageDialog(null, message, "Error",
				JOptionPane.ERROR_MESSAGE);
	}

	/**
	 * Fills the textField texts with the selected for editing customer's info.
	 */
	private void populateTxtFieldsWIthCustomerInfo(Customer selectedCustomer) {
		editCustomerView.setTxtFirstName(selectedCustomer.getFirstName());
		editCustomerView.setTxtLastName(selectedCustomer.getLastName());
		editCustomerView.setTxtTimeWorked(new Stopwatch()
				.getElapsedTimeStringFromMillis(selectedCustomer
						.getElapsedTimeInMillis()));
		editCustomerView.setTxtChargePerHour(String.valueOf(selectedCustomer
				.getChargePerHour()));
	}
}
