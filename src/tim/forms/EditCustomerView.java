package tim.forms;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

/**
 * 
 * @author <code>Eleni Aidonidou</code>
 *
 */
@SuppressWarnings("serial")
public class EditCustomerView extends JFrame {
	/**
	 * The selected customer Id the frame needs to operate.
	 */
	private int selectedCustomerId;

	/**
	 * Getter & Setter for selectedCustomerId.
	 */
	public int getSelectedCustomerId() {
		return selectedCustomerId;
	}

	public void setSelectedCustomerId(int selectedCustomerId) {
		this.selectedCustomerId = selectedCustomerId;
	}

	/**
	 * The necessary textFields that are used to display/change the customers
	 * info.
	 */
	private JTextField txtFirstName;
	private JTextField txtLastName;
	private JTextField txtTimeWorked;
	private JTextField txtChargePerHour;

	/**
	 * Getters & Setters for the textFields text.
	 */
	public String getTxtFirstName() {
		return txtFirstName.getText();
	}

	public void setTxtFirstName(String txtFirstName) {
		this.txtFirstName.setText(txtFirstName);
	}

	public String getTxtLastName() {
		return txtLastName.getText();
	}

	public void setTxtLastName(String txtLastName) {
		this.txtLastName.setText(txtLastName);
		;
	}

	public String getTxtTimeWorked() {
		return txtTimeWorked.getText();
	}

	public void setTxtTimeWorked(String txtTimeWorked) {
		this.txtTimeWorked.setText(txtTimeWorked);
	}

	public String getTxtChargePerHour() {
		return txtChargePerHour.getText();
	}

	public void setTxtChargePerHour(String txtChargePerHour) {
		this.txtChargePerHour.setText(txtChargePerHour);
	}

	/**
	 * The necessary buttons.
	 */
	private JButton btnSave;
	private JButton btnResetTimeWorked;
	private JButton btnCancel;

	/**
	 * Methods that are used by the controller to add Action Listeners to the
	 * buttons.
	 */
	public void addBtnSaveActionListener(ActionListener listener) {
		btnSave.addActionListener(listener);
	}

	public void addBtnResetTimeWorkedActionListener(ActionListener listener) {
		btnResetTimeWorked.addActionListener(listener);
	}

	public void addBtnCancelActionListener(ActionListener listener) {
		btnCancel.addActionListener(listener);
	}

	/**
	 * Panel used to display the components in a group.
	 */
	private JPanel contentPane;
	private JPanel pnlCustomer;
	/**
	 * Static Title Labels.
	 */
	private JLabel lblFirstNameTitle;
	private JLabel lblLastNameTitle;
	private JLabel lblTimeWorkedTitle;
	private JLabel lblChargePerHourTitle;

	/**
	 * Constructor that creates the EditCustomerView frame.
	 */
	public EditCustomerView() {
		this.setResizable(false);
		this.setType(Type.UTILITY);
		this.setTitle("Edit Customer");
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setBounds(100, 100, 391, 200);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.setContentPane(contentPane);
		contentPane.setLayout(null);

		pnlCustomer = new JPanel();
		pnlCustomer.setBounds(10, 11, 365, 117);
		contentPane.add(pnlCustomer);
		pnlCustomer.setLayout(null);

		lblFirstNameTitle = new JLabel("First Name:");
		lblFirstNameTitle.setBounds(10, 14, 107, 14);
		pnlCustomer.add(lblFirstNameTitle);

		lblLastNameTitle = new JLabel("Last Name:");
		lblLastNameTitle.setBounds(10, 39, 107, 14);
		pnlCustomer.add(lblLastNameTitle);

		lblTimeWorkedTitle = new JLabel("Time Worked:");
		lblTimeWorkedTitle.setBounds(10, 64, 107, 14);
		pnlCustomer.add(lblTimeWorkedTitle);

		lblChargePerHourTitle = new JLabel("Charge/Hour:");
		lblChargePerHourTitle.setBounds(10, 89, 107, 14);
		pnlCustomer.add(lblChargePerHourTitle);

		txtFirstName = new JTextField();
		txtFirstName.setBounds(144, 11, 211, 20);
		pnlCustomer.add(txtFirstName);
		txtFirstName.setColumns(10);

		txtLastName = new JTextField();
		txtLastName.setBounds(144, 36, 211, 20);
		pnlCustomer.add(txtLastName);
		txtLastName.setColumns(10);

		txtTimeWorked = new JTextField();
		txtTimeWorked.setEditable(false);
		txtTimeWorked.setBounds(144, 61, 211, 20);
		pnlCustomer.add(txtTimeWorked);
		txtTimeWorked.setColumns(10);

		txtChargePerHour = new JTextField();
		txtChargePerHour.setBounds(144, 86, 211, 20);
		pnlCustomer.add(txtChargePerHour);
		txtChargePerHour.setColumns(10);

		btnSave = new JButton("Save");
		btnSave.setBounds(10, 139, 89, 23);
		contentPane.add(btnSave);

		btnResetTimeWorked = new JButton("Reset Time Worked");
		btnResetTimeWorked.setBounds(109, 139, 167, 23);
		contentPane.add(btnResetTimeWorked);

		btnCancel = new JButton("Cancel");
		btnCancel.setBounds(286, 139, 89, 23);
		contentPane.add(btnCancel);
	}

}
