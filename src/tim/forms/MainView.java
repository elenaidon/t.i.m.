package tim.forms;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ItemListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.border.EtchedBorder;

import tim.controller.ClockListener;

/**
 * 
 * @author <code>Eleni Aidonidou</code>
 *
 */
@SuppressWarnings("serial")
public class MainView extends JFrame {
	/**
	 * A timer object for counting and displaying the elapsed time.
	 */
	private Timer timer;
	
	/**
	 * Resets the timer object by replacing it with a new one.
	 * 
	 * @param newTimer
	 *            A fresh timer object.
	 */
	public void setTimer(Timer newTimer) {
		timer = newTimer;
		timer.setInitialDelay(0);
	}

	/**
	 * Method used by the controller to check if the timer is running.
	 * 
	 * @return <code>true</code> if the timer is running; <code>false</code> if
	 *         it's not.
	 */
	public boolean isTimerRunning() {
		return timer.isRunning();
	}

	/**
	 * Used by the controller to start the timer.
	 */
	public void startTimer() {
		timer.start();
	}

	/**
	 * Used by the controller to stop the timer.
	 */
	public void stopTimer() {
		timer.stop();
	}

	/**
	 * Used by the controller to add an Action Listener to the timer object.
	 * 
	 * @param listener
	 *            An Action Listener that controls what happens its time the
	 *            timer "ticks".
	 */
	public void addTimerActionListener(ClockListener listener) {
		timer.addActionListener(listener);
	}

	/**
	 * The necessary buttons.
	 */
	private JToggleButton tglbtnStart;
	private JButton btnManageCustomers;
	private JButton btnSave;
	private JButton btnReset;

	/**
	 * Methods that are used by the controller to add Action Listeners to the
	 * buttons.
	 */
	public void addTglBtnStartItemListener(ItemListener listener) {
		this.tglbtnStart.addItemListener(listener);
	}

	public void addBtnManageCustomersActionListener(ActionListener listener) {
		this.btnManageCustomers.addActionListener(listener);
	}

	public void addBtnSaveActionListener(ActionListener listener) {
		this.btnSave.addActionListener(listener);
	}

	public void addBtnResetActionListener(ActionListener listener) {
		this.btnReset.addActionListener(listener);
	}

	/**
	 * Method that returns if the toggle button is selected.
	 */
	public boolean isTglBtnStartSelected() {
		return tglbtnStart.isSelected();
	}

	/**
	 * Method that Sets the Text on the toggle button.
	 * 
	 * @param text
	 *            The new text that is about to be set.
	 */
	public void setTglBtnStartText(String text) {
		tglbtnStart.setText(text);
	}

	/**
	 * Dynamic Labels are used to display changing info.
	 */
	private JLabel lblCurrentCustomerName;
	private JLabel lblTimeWorked;
	private JLabel lblChargePerHour;
	private JLabel lblDebt;
	private JLabel lblElapsedTime;
	/**
	 * Getters and Setters for the dynamic Labels.
	 */
	public void setCurrentCustomerFullName(String customerFullName) {
		lblCurrentCustomerName.setText(customerFullName);
	}

	public String getLblCurrentCustomerFullNameText() {
		return lblCurrentCustomerName.getText();
	}

	public void setCurrentCustomerTimeWorked(String customerTimeWorked) {
		lblTimeWorked.setText(customerTimeWorked);
	}

	public void setCurrentCustomerChargePerHour(String customerChargePerHour) {
		lblChargePerHour.setText(customerChargePerHour);
	}

	public void setCurrentCustomerDebt(String currentCustomerDebt) {
		lblDebt.setText(currentCustomerDebt);
	}

	public void setElapsedTimeString(String elapsedStopwatchTime) {
		lblElapsedTime.setText(elapsedStopwatchTime);
	}

	public String getElapsedTimeString() {
		return lblElapsedTime.getText();
	}
	
	/**
	 * Static Title Labels.
	 */
	private JLabel lblCurrentCustomerTitle;
	private JLabel lblTimeWorkedTitle;
	private JLabel lblChargePerHourTitle;
	private JLabel lblDebtTitle;
	private JLabel lblElapsedTimeTitle;

	/**
	 * Panels used to display the components in groups.
	 */
	private JPanel pnlCustomer;
	private JPanel pnlStopwatch;

	/**
	 * Constructor that creates the MainView frame.
	 */
	public MainView() {
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setTimer(new Timer(0, null));
		this.getContentPane().setBackground(Color.WHITE);
		this.setTitle("T.i.M. (Time is Money)");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setBounds(100, 100, 454, 297);
		this.getContentPane().setLayout(null);

		pnlCustomer = new JPanel();
		pnlCustomer.setBounds(10, 11, 430, 252);
		this.getContentPane().add(pnlCustomer);
		pnlCustomer.setLayout(null);

		lblCurrentCustomerTitle = new JLabel("Current Customer:");
		lblCurrentCustomerTitle.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblCurrentCustomerTitle.setBounds(10, 5, 107, 16);
		lblCurrentCustomerTitle.setHorizontalAlignment(SwingConstants.LEFT);
		pnlCustomer.add(lblCurrentCustomerTitle);

		lblCurrentCustomerName = new JLabel();
		lblCurrentCustomerName.setFont(new Font("Tahoma", Font.ITALIC, 15));
		lblCurrentCustomerName.setBounds(127, 5, 268, 16);
		pnlCustomer.add(lblCurrentCustomerName);

		lblTimeWorkedTitle = new JLabel("Time Worked:");
		lblTimeWorkedTitle.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblTimeWorkedTitle.setBounds(10, 32, 82, 16);
		pnlCustomer.add(lblTimeWorkedTitle);

		lblTimeWorked = new JLabel();
		lblTimeWorked.setFont(new Font("Tahoma", Font.ITALIC, 15));
		lblTimeWorked.setBounds(127, 32, 268, 16);
		pnlCustomer.add(lblTimeWorked);

		btnManageCustomers = new JButton("Manage Customers");
		btnManageCustomers.setBounds(10, 184, 170, 23);
		pnlCustomer.add(btnManageCustomers);

		btnSave = new JButton("Save");
		btnSave.setBounds(10, 218, 170, 23);
		pnlCustomer.add(btnSave);

		pnlStopwatch = new JPanel();
		pnlStopwatch.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null,
				null));
		pnlStopwatch.setBounds(206, 75, 214, 166);
		pnlCustomer.add(pnlStopwatch);
		pnlStopwatch.setLayout(null);

		lblElapsedTimeTitle = new JLabel("Elapsed Time");
		lblElapsedTimeTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lblElapsedTimeTitle.setFont(new Font("Tahoma", Font.ITALIC, 20));
		lblElapsedTimeTitle.setBounds(10, 11, 194, 25);
		pnlStopwatch.add(lblElapsedTimeTitle);

		lblElapsedTime = new JLabel("00:00:00:00");
		lblElapsedTime.setHorizontalAlignment(SwingConstants.CENTER);
		lblElapsedTime.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblElapsedTime.setBounds(10, 47, 194, 45);
		pnlStopwatch.add(lblElapsedTime);

		tglbtnStart = new JToggleButton();
		tglbtnStart.setBounds(49, 103, 109, 23);
		pnlStopwatch.add(tglbtnStart);

		btnReset = new JButton("Reset");
		btnReset.setBounds(49, 137, 109, 23);
		pnlStopwatch.add(btnReset);

		lblChargePerHourTitle = new JLabel("Charge/Hour:");
		lblChargePerHourTitle.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblChargePerHourTitle.setBounds(10, 59, 99, 16);
		pnlCustomer.add(lblChargePerHourTitle);

		lblChargePerHour = new JLabel("");
		lblChargePerHour.setFont(new Font("Tahoma", Font.ITALIC, 15));
		lblChargePerHour.setBounds(127, 59, 193, 16);
		pnlCustomer.add(lblChargePerHour);

		lblDebtTitle = new JLabel("Debt:");
		lblDebtTitle.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblDebtTitle.setBounds(10, 94, 107, 14);
		pnlCustomer.add(lblDebtTitle);

		lblDebt = new JLabel("");
		lblDebt.setBackground(Color.WHITE);
		lblDebt.setHorizontalAlignment(SwingConstants.CENTER);
		lblDebt.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblDebt.setBounds(10, 119, 170, 45);
		pnlCustomer.add(lblDebt);
	}

}
