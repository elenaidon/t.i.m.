package tim.forms;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;

import tim.code.Customer;

/**
 * 
 * @author <code>Eleni Aidonidou</code>
 *
 */
@SuppressWarnings("serial")
public class CustomersView extends JFrame {
	/**
	 * Static variable with the euro sign in Java language.
	 */
	public final static String euro = "\u20ac";
	/**
	 * The Customer that is currently selected if any.
	 */
	private Customer currentCustomer;

	/**
	 * Setter & Getter for the currentCustomer variable.
	 */
	public void setCurrentCustomer(Customer currentCustomer) {
		this.currentCustomer = currentCustomer;
	}

	public Customer getCurrentCustomer() {
		return currentCustomer;
	}

	/**
	 * Panels used to display the components in groups.
	 */
	private JPanel contentPane;
	private JPanel pnlCustomerTable;
	private JPanel pnlCurrentCustomer;
	/**
	 * Scroll Panel is used so that the data in the Table will be displayed in a
	 * scrollable list.
	 */
	private JScrollPane scrollPane;

	/**
	 * The necessary buttons.
	 */
	private JButton btnAddCustomer;
	private JButton btnRemoveCustomer;
	private JButton btnEditCustomer;
	private JButton btnSelectCustomer;
	private JButton btnCancel;

	/**
	 * Methods that are used by the controller to add Action Listeners to the
	 * buttons.
	 */
	public void addBtnAddCustomerActionListener(ActionListener listener) {
		btnAddCustomer.addActionListener(listener);
	}

	public void addBtnRemoveCustomerActionListener(ActionListener listener) {
		btnRemoveCustomer.addActionListener(listener);
	}

	public void addBtnEditCustomerActionListener(ActionListener listener) {
		btnEditCustomer.addActionListener(listener);
	}

	public void addBtnSelectCustomerActionListener(ActionListener listener) {
		btnSelectCustomer.addActionListener(listener);
	}

	public void addBtnCancelActionListener(ActionListener listener) {
		btnCancel.addActionListener(listener);
	}

	/**
	 * A JTable that displays the customers that exist in the Database.
	 */
	private JTable customerTable;

	/**
	 * Getter that exports the Customers JTable.
	 */
	public JTable getCustomerTable() {
		return this.customerTable;
	}

	/**
	 * Sets the Customers JTable Model to match the requirements of the
	 * application.
	 * 
	 * @param model
	 *            the desired model to be set.
	 */
	public void setTableModel(DefaultTableModel model) {
		customerTable.setModel(model);
	}

	
	/**
	 * TextField used to search char sequences in either first or last name.
	 */
	private JTextField txtSearchCustomer;
	
	/**
	 * Getter & Setter for the text of the Search Customer Text Field.
	 */
	public String getTxtSearchCustomer() {
		return txtSearchCustomer.getText();
	}

	public void setTxtSearchCustomer(String txtSearchCustomer) {
		this.txtSearchCustomer.setText(txtSearchCustomer);
	}

	/**
	 * Method that adds an Action Listener to the search textField.
	 * 
	 * @param listener
	 *            Defines what happens when text is entered in the textField.
	 */
	public void addTxtSearchCustomerDocumentListener(DocumentListener listener) {
		txtSearchCustomer.getDocument().addDocumentListener(listener);
	}

	/**
	 * Dynamic Label is used to display changing info.
	 */
	private JLabel lblCurrentCustomerName;

	/**
	 * Getter & Setter for the Dynamic Label
	 */
	public void setLblCurrentCustomerName(String lblCurrentCustomerName) {
		this.lblCurrentCustomerName.setText(lblCurrentCustomerName);
	}

	public String getLblCurrentCustomerName() {
		return lblCurrentCustomerName.getText();
	}

	/**
	 * Static Title Labels.
	 */
	private JLabel lblSearchCustomer;
	private JLabel lblCurrentCustomerTitle;

	/**
	 * Constructor that creates the MainViewCustomersView frame.
	 */
	public CustomersView() {
		this.setResizable(false);
		this.setTitle("Customers");
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setBounds(100, 100, 569, 444);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.setContentPane(contentPane);
		contentPane.setLayout(null);

		pnlCustomerTable = new JPanel();
		pnlCustomerTable.setBounds(10, 64, 533, 302);
		contentPane.add(pnlCustomerTable);
		pnlCustomerTable.setLayout(null);

		lblSearchCustomer = new JLabel("Search Customer");
		lblSearchCustomer.setBounds(10, 45, 112, 19);
		pnlCustomerTable.add(lblSearchCustomer);
		lblSearchCustomer.setFont(new Font("Tahoma", Font.PLAIN, 15));

		txtSearchCustomer = new JTextField();
		txtSearchCustomer.setBounds(132, 46, 391, 20);
		pnlCustomerTable.add(txtSearchCustomer);
		txtSearchCustomer.setColumns(10);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 75, 513, 216);
		pnlCustomerTable.add(scrollPane);
		customerTable = new JTable();
		scrollPane.setViewportView(customerTable);
		customerTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		btnAddCustomer = new JButton("Add Customer");
		btnAddCustomer.setBounds(10, 11, 160, 23);
		pnlCustomerTable.add(btnAddCustomer);

		btnRemoveCustomer = new JButton("Remove Customer");
		btnRemoveCustomer.setBounds(363, 11, 160, 23);
		pnlCustomerTable.add(btnRemoveCustomer);

		btnEditCustomer = new JButton("Edit Customer");
		btnEditCustomer.setBounds(186, 11, 160, 23);
		pnlCustomerTable.add(btnEditCustomer);

		btnSelectCustomer = new JButton("Select Customer");
		btnSelectCustomer.setBounds(10, 377, 205, 23);
		contentPane.add(btnSelectCustomer);

		btnCancel = new JButton("Cancel");
		btnCancel.setBounds(338, 377, 205, 23);
		contentPane.add(btnCancel);

		pnlCurrentCustomer = new JPanel();
		pnlCurrentCustomer.setBounds(10, 11, 533, 42);
		contentPane.add(pnlCurrentCustomer);
		pnlCurrentCustomer.setLayout(null);

		lblCurrentCustomerTitle = new JLabel("Current Customer:");
		lblCurrentCustomerTitle.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblCurrentCustomerTitle.setBounds(10, 11, 121, 19);
		pnlCurrentCustomer.add(lblCurrentCustomerTitle);

		lblCurrentCustomerName = new JLabel("No Customer Selected");
		lblCurrentCustomerName.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblCurrentCustomerName.setBounds(141, 11, 269, 19);
		pnlCurrentCustomer.add(lblCurrentCustomerName);
	}

}
