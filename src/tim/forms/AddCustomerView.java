package tim.forms;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

/**
 * 
 * @author <code>Eleni Aidonidou</code>
 *
 */
@SuppressWarnings("serial")
public class AddCustomerView extends JFrame {
	/**
	 * The necessary textFields that are used to display/change the customers
	 * info.
	 */
	private JTextField txtFirstName;
	private JTextField txtLastName;
	private JTextField txtChargePerhour;

	/**
	 * Getters & Setters for the textFields text.
	 */
	public void setTxtFirstName(String newFirstName) {
		txtFirstName.setText(newFirstName);
	}

	public String getTxtFirstName() {
		return txtFirstName.getText();
	}

	public void setTxtLastName(String newLastName) {
		txtLastName.setText(newLastName);
	}

	public String getTxtLastName() {
		return txtLastName.getText();
	}

	public void setTxtChargePerhour(String newChargePerHour) {
		txtChargePerhour.setText(newChargePerHour);
	}

	public String getTxtChargePerHour() {
		return txtChargePerhour.getText();
	}

	/**
	 * The necessary buttons.
	 */
	private JButton btnReset;
	private JButton btnAdd;
	private JButton btnCancel;

	/**
	 * Methods that are used by the controller to add Action Listeners to the
	 * buttons.
	 */
	public void addBtnResetActionListener(ActionListener listener) {
		btnReset.addActionListener(listener);
	}

	public void addBtnAddActionListener(ActionListener listener) {
		btnAdd.addActionListener(listener);
	}

	public void addBtnCancelActionListener(ActionListener listener) {
		btnCancel.addActionListener(listener);
	}

	/**
	 * Panels used to display the components in groups.
	 */
	private JPanel contentPane;
	private JPanel pnlCustomer;
	/**
	 * Static Title Labels.
	 */
	private JLabel lblFirstNameTitle;
	private JLabel lblLastNameTitle;
	private JLabel lblChargePerHourTitle;

	/**
	 * Constructor that creates the AddCustomerView frame.
	 */
	public AddCustomerView() {
		this.setResizable(false);
		this.setType(Type.UTILITY);
		this.setTitle("New Customer");
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setBounds(100, 100, 343, 177);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.setContentPane(contentPane);
		contentPane.setLayout(null);

		pnlCustomer = new JPanel();
		pnlCustomer.setBounds(10, 11, 312, 129);
		contentPane.add(pnlCustomer);
		pnlCustomer.setLayout(null);

		lblFirstNameTitle = new JLabel("First Name:");
		lblFirstNameTitle.setBounds(10, 14, 95, 14);
		pnlCustomer.add(lblFirstNameTitle);

		txtFirstName = new JTextField();
		txtFirstName.setBounds(115, 11, 184, 20);
		pnlCustomer.add(txtFirstName);
		txtFirstName.setColumns(10);

		lblLastNameTitle = new JLabel("Last Name:");
		lblLastNameTitle.setBounds(10, 39, 95, 14);
		pnlCustomer.add(lblLastNameTitle);

		txtLastName = new JTextField();
		txtLastName.setBounds(115, 36, 184, 20);
		pnlCustomer.add(txtLastName);
		txtLastName.setColumns(10);

		btnReset = new JButton("Reset");
		btnReset.setBounds(10, 92, 89, 23);
		pnlCustomer.add(btnReset);

		btnAdd = new JButton("Add");
		btnAdd.setBounds(111, 92, 89, 23);
		pnlCustomer.add(btnAdd);

		btnCancel = new JButton("Cancel");
		btnCancel.setBounds(210, 92, 89, 23);
		pnlCustomer.add(btnCancel);

		lblChargePerHourTitle = new JLabel("Charge/Hour:");
		lblChargePerHourTitle.setBounds(10, 64, 95, 14);
		pnlCustomer.add(lblChargePerHourTitle);

		txtChargePerhour = new JTextField();
		txtChargePerhour.setBounds(115, 61, 184, 20);
		pnlCustomer.add(txtChargePerhour);
		txtChargePerhour.setColumns(10);
	}


}
