package tim.code;

import java.text.DecimalFormat;
import java.util.concurrent.TimeUnit;

public class Stopwatch {
	private long elapsedTimeInMillis;
	private String days;
	private String hours;
	private String minutes;
	private String seconds;
	private String elapsedTimeFormatedString;

	// private Timer timer;

	public Stopwatch() {
		setElapsedTimeFormatedString("00:00:00:00");
		setElapsedTimeInMillis(0);
	}

	public String getElapsedTimeFormatedString() {
		return elapsedTimeFormatedString;
	}

	public void setElapsedTimeFormatedString(String elapsedTimeFormatedString) {
		this.elapsedTimeFormatedString = elapsedTimeFormatedString;
	}

	public long getElapsedTimeInMillis() {
		return elapsedTimeInMillis;
	}

	public void setElapsedTimeInMillis(long elapsedTimeInMillis) {
		this.elapsedTimeInMillis = elapsedTimeInMillis;
		createElapsedTimeFormattedString();
	}

	public void convertToHoursMinutesSeconds() {
		Long tempElapsedTimeInMillis = elapsedTimeInMillis;
		this.days = decimalFormat(TimeUnit.MILLISECONDS
				.toDays(tempElapsedTimeInMillis));
		tempElapsedTimeInMillis -= TimeUnit.DAYS.toMillis(Long.parseLong(days));
		this.hours = decimalFormat(TimeUnit.MILLISECONDS
				.toHours(tempElapsedTimeInMillis));
		tempElapsedTimeInMillis -= TimeUnit.HOURS.toMillis(Long
				.parseLong(hours));
		this.minutes = decimalFormat(TimeUnit.MILLISECONDS
				.toMinutes(tempElapsedTimeInMillis));
		tempElapsedTimeInMillis -= TimeUnit.MINUTES.toMillis(Long
				.parseLong(minutes));
		this.seconds = decimalFormat(TimeUnit.MILLISECONDS
				.toSeconds(tempElapsedTimeInMillis));
	}
	 
	public void createElapsedTimeFormattedString() {
		convertToHoursMinutesSeconds();
		elapsedTimeFormatedString = new StringBuffer().append(days).append(":")
				.append(hours)
				.append(":").append(minutes).append(":").append(seconds)
				.toString();
	}

	public static String decimalFormat(long value) {
		DecimalFormat nft = new DecimalFormat("#00.###");
		nft.setDecimalSeparatorAlwaysShown(false);
		return nft.format(value);
	}
	
	public String getElapsedTimeStringFromMillis(long givenMillis) {
		setElapsedTimeInMillis(givenMillis);
		createElapsedTimeFormattedString();
		return getElapsedTimeFormatedString();
	}
}
