package tim.code;

import java.util.concurrent.TimeUnit;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CUSTOMER")
public class Customer {
	@Id
	@GeneratedValue
	@Column(name = "id")
	private int id;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "elapsed_millis")
	private long elapsedTimeInMillis;

	@Column(name = "charge_per_hour")
	private double chargePerHour;

	public Customer() {
		setId(0);
		setLastName("No");
		setFirstName("Customer Selected");
		setElapsedTimeInMillis(0);
		setChargePerHour(0);
	}

	/**
	 * @param firstName
	 * @param lastName
	 * @param elapsedTimeInMillis
	 * @param chargePerHour
	 */
	public Customer(String firstName, String lastName,
			long elapsedTimeInMillis, double chargePerHour) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.elapsedTimeInMillis = elapsedTimeInMillis;
		this.chargePerHour = chargePerHour;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String newFirstName) {
		this.firstName = newFirstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String newLastName) {
		this.lastName = newLastName;
	}

	public long getElapsedTimeInMillis() {
		return elapsedTimeInMillis;
	}

	public void setElapsedTimeInMillis(long newElapsedTimeInMillis) {
		this.elapsedTimeInMillis = newElapsedTimeInMillis;
	}

	public double getChargePerHour() {
		return chargePerHour;
	}

	public void setChargePerHour(double chargePerHour) {
		this.chargePerHour = chargePerHour;
	}

	public String getFullName() {
		StringBuilder sb = new StringBuilder();
		sb.append(getLastName());
		sb.append(" ");
		sb.append(getFirstName());
		return sb.toString();
	}

	public Double getDebt() {
		Double chargePerMillisecond = chargePerHour
				/ TimeUnit.HOURS.toMillis(1);
		Double totalCharge = elapsedTimeInMillis * chargePerMillisecond;
		return (double) Math.round(totalCharge * 1000) / 1000;
	}
}
