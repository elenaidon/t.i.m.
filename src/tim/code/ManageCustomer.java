package tim.code;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;
import org.hibernate.service.ServiceRegistry;

public class ManageCustomer {
	private static SessionFactory factory; 
	private static SessionFactory sessionFactory;
	private static ServiceRegistry serviceRegistry;
	
	public ManageCustomer() {
		try {

			factory = createSessionFactory();

		} catch (Throwable ex) {
			System.err.println("Failed to create sessionFactory object." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public void run() {

		ManageCustomer ME = new ManageCustomer();

		/* Add few employee records in database */
		Integer empID1 = ME.addCustomer("Zara", "Ali", 1000, 10);
		Integer empID2 = ME.addCustomer("Daisy", "Das", 5000, 2);
		Integer empID3 = ME.addCustomer("John", "Paul", 10000, 3);

		/* List down all the employees */
		ME.listCustomers();

		/* Update employee's records */
		ME.updateCustomerElapsedMillis(empID1, 5000);

		/* Delete an employee from the database */
		ME.deleteCustomer(empID2);

		/* List down new list of the employees */
		ME.listCustomers();
	}

	/* Method to create the SessionFactory */
	public static SessionFactory createSessionFactory() {
		Configuration configuration = new Configuration();
		configuration.configure().addAnnotatedClass(Customer.class);
		serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
				configuration.getProperties()).build();
		sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		return sessionFactory;
	}

	/* Method to CREATE a customer in the database */
	public Integer addCustomer(String firstName, String lastName,
			long elapsedInMillis, double chargePerHour) {
		Session session = factory.openSession();
		Transaction tx = null;
		Integer customerID = null;
		try {
			tx = session.beginTransaction();
			Customer customer = new Customer();
			customer.setFirstName(firstName);
			customer.setLastName(lastName);
			customer.setElapsedTimeInMillis(elapsedInMillis);
			customer.setChargePerHour(chargePerHour);
			customerID = (Integer) session.save(customer);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			// e.printStackTrace();
		} finally {
			session.close();
		}
		return customerID;
	}

	/* Method to DELETE a customer from the records */
	public void deleteCustomer(Integer customerID) {
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			Customer customer = (Customer) session.get(Customer.class,
					customerID);
			session.delete(customer);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			// e.printStackTrace();
		} finally {
			session.close();
		}
	}

	/* Method to UPDATE elapsed millis for a customer */
	public void updateCustomerElapsedMillis(Integer customerID, long elapsedInMillis) {
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			Customer customer = (Customer) session.get(Customer.class,
					customerID);
			customer.setElapsedTimeInMillis(elapsedInMillis);
			session.update(customer);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			// e.printStackTrace();
		} finally {
			session.close();
		}
	}

	/* Method to UPDATE charge per hour for a customer */
	public void updateCustomerChargePerHour(Integer customerID, double chargePerHour) {
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			Customer customer = (Customer) session.get(Customer.class,
					customerID);
			customer.setChargePerHour(chargePerHour);
			session.update(customer);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			// e.printStackTrace();
		} finally {
			session.close();
		}
	}

	/* Method to update a customers info in the database */
	public void updateCustomerInfo(Integer customerID, String firstName,
			String lastName,
			double chargePerHour) {
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			Customer customer = (Customer) session.get(Customer.class,
					customerID);
			customer.setFirstName(firstName);
			customer.setLastName(lastName);
			customer.setChargePerHour(chargePerHour);
			session.update(customer);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			// e.printStackTrace();
		} finally {
			session.close();
		}
	}

	/* Method to READ all the customers */
	public void listCustomers() {
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			List customers = session.createQuery("FROM Customer").list();

			for (Iterator iterator = customers.iterator(); iterator.hasNext();) {
				Customer customer = (Customer) iterator.next();
				System.out.print("First Name: " + customer.getFirstName());
				System.out.print("  Last Name: " + customer.getLastName());
				System.out.println("  Elapsed Time in Milliseconds: "
						+ customer.getElapsedTimeInMillis());
				System.out.println(" Charge Per Hour: "
						+ customer.getChargePerHour());
			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			// e.printStackTrace();
		} finally {
			session.close();
		}
	}

	/*
	 * Method to search the customers that have the string given in their last
	 * or first name
	 */
	public List<Customer> findCustomer(String partialName) {
		Session session = factory.openSession();
		List<Customer> resultCustomers = new ArrayList<Customer>();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			List customers = session.createQuery("FROM Customer").list();

			for (Iterator iterator = customers.iterator(); iterator.hasNext();) {
				Customer customer = (Customer) iterator.next();
				String fullName = customer.getFirstName() + " "
						+ customer.getLastName();
				if (fullName.toUpperCase().contains(partialName.toUpperCase())) {
					resultCustomers.add(customer);
				}
			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			// e.printStackTrace();
		} finally {
			session.close();
		}
		return resultCustomers;
	}

	/* Method to return a list with all the customers */
	public List<Customer> getCustomerList() {
		List customers = new ArrayList<Customer>();
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			customers = session.createQuery("FROM Customer").list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			// e.printStackTrace();
		} finally {
			session.close();
		}
		return customers;
	}

	/* Method to to RETURN the customer with the given id */
	public Customer getCustomerById(int requestedId) {
		Customer customer = new Customer();
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			Criteria cr = session.createCriteria(Customer.class);
			// Add restriction.
			cr.add(Restrictions.gt("id", requestedId - 1));

			customer = (Customer) cr.list().get(0);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			// e.printStackTrace();
		} finally {
			session.close();
		}
		return customer;
	}
}
